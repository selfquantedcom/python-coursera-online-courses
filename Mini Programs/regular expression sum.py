run=open("C:\Users\Frantisek\Desktop\Python\\sampledata.txt")
import re
my_list=[]

for line in run:
    line=line.strip()
    x=re.findall("([0-9]+)", line)
#the line above will extract all numbers on a line and put them into a list.
#We might therefore get something like x= ['800', '500' ...].
#If we add this to my_list,with the intention of summing up the elements
#later on, it will not be possible
#as we will get something like my_list= [ ['800', '500'], ['400', '500'],...].
#This is a list with multiple sub lists, each containing multiple elements :)
#we can't do much computing with that. The following code takes care of this and
#ensures, that each number is added as one element to my_list so that we get
#something like my_list= ['800', 500, 400, ....]
    if len(x)>0:
        for item in range(0,len(x)):
            my_list.append(x[item])
#Now we have a nice list full of strings however :). We can therefore not
#use the function sum, to get the total. The easiest way I found was to convert
#the individual elements into intigers using a for loop and to "accumulate"
#them onto a variable called "total" in order to get the sum.
#The following code does that.         
total=0
for item in my_list:
    item=int(item)
    total=total+item
print total


    

           





            

    
