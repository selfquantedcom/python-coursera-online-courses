try:
    x=float(raw_input("Enter a number between 0.0 and 1.0:"))
except:
    x=-1
if x>1:
    print"Bad score"
elif x>=0.9:
    print"A"
elif x>=0.8:
    print"B"
elif x>=0.7:
    print"C"
elif x>=0.6:
    print"D"
elif x>=0:
    print"F"
else:
    print"Bad score"
