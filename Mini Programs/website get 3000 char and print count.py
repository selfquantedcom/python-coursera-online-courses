import socket
import re
user_input=raw_input("Enter a URL: ") #http://www.py4inf.com/code/romeo.txt

request="GET "+user_input+" HTTP/1.0\n\n" #use to input in "send" method
try:
    user_input=re.findall("^http://(www.*?)/",user_input)
    user_input=user_input[0] #use to input in "connect" method
except:
    print "Wrong URL"
    quit()

mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mysock.connect((user_input, 80))
mysock.send(request)


data = mysock.recv(3000)

print data
print len(data)

mysock.close()
