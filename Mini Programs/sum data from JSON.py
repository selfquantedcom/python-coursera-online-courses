import json
import urllib
import string

url = raw_input('Enter website: ')
if len(url) < 1 : url="http://python-data.dr-chuck.net/comments_287565.json?"

print 'Retrieving', url
uh = urllib.urlopen(url)
data = uh.read()
data=str(data)
try:
    js = json.loads(str(data))
except:
    js = None
    print "incorrect data"

my_list=[]  
data=json.dumps(js, indent=4)
data=data.strip().split()
   
for line in data:
    line=line.translate(None, string.punctuation)
    try:
        line=int(line)
        my_list.append(line)
    except: continue
print sum(my_list)
