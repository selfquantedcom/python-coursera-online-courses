import urllib
from bs4 import BeautifulSoup

text_file = open("Output.txt", "w")

def extract_text():
    #write text from html to output file
    for p_tag in soup.find_all('p'):
        data = p_tag.text
        if(data.startswith("Copyright")):
            break
        else:
            text_file.write(p_tag.text)
    return

#extract text from first page
req = urllib.request.Request("http://www.onlinebook4u.net/mystery/Theodore_Boone_4/index.html", headers={'User-Agent' : "Magic Browser"}) 
page = urllib.request.urlopen(req)
soup = BeautifulSoup(page, 'html.parser')
extract_text()

#extract remaining pages
for number in range(2,5):
    #establish url ocnnection
    req = urllib.request.Request("http://www.onlinebook4u.net/mystery/Theodore_Boone_4/index_" + str(number) + ".html", headers={'User-Agent' : "Magic Browser"}) 
    page = urllib.request.urlopen(req)
    soup = BeautifulSoup(page, 'html.parser')
    extract_text()   

text_file.close()