import urllib
import json

serviceurl = 'http://python-data.dr-chuck.net/geojson?'

while True:
    address = raw_input('Enter location: ')
    if len(address) < 1 : address="South Federal University"

    url = serviceurl + urllib.urlencode({'sensor':'false', 'address': address})
    uh = urllib.urlopen(url)
    data = uh.read()
    

    try: js = json.loads(str(data))
    except: js = None
    if 'status' not in js or js['status'] != 'OK':
        print '==== Failure To Retrieve ===='
        continue

    print json.dumps(js, indent=4)
    try:
        identi = js["results"][0]["place_id"]
        print identi
    except:
        print "Error"

   

