try:
    score=float(raw_input("Enter a number between 0.0 and 1.0:"))
except:
    score=-1

def computegrade(score):
    if score>1:
        grade="Bad score"
        print grade
    elif score>=0.9:
        grade="A"
        print grade
    elif score>=0.8:
        grade="B"
        print grade
    elif score>=0.7:
        grade="C"
        print grade
    elif score>=0.6:
        grade="D"
        print grade
    elif score>=0:
        grade="F"
        print grade
    else:
        grade="Bad score"
        print grade
    return grade

computegrade(score)
