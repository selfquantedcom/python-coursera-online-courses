#INFO:
#use loc[row,column] dispaly a section of a df
#use df.set_value('row', 'column', value) to change a particular value in df
#use df.iloc[row,column] to get a particular value 
# use df.index.names = ['xxx'] to rename index 
# use df.columns = ['a', 'b'] to rename columns

import pandas as pd
import numpy as np
def answer_one():
    ##Data cleaning##
    
    #read the file and skip headers and footers
    energy = pd.read_excel("Energy Indicators.xls", header = 18, skip_footer=38, index_col=None)
    #give names to columns and drop unnecessary columns
    energy.columns=["Country","DEL","Energy Supply","Energy Supply per Capita","% Renewable" ]
    del(energy["DEL"])
    energy = energy.reset_index()
    del(energy["index"])
    #identify missing values
    energy = energy.replace("...", np.nan)
    #change units
    energy.loc[:,'Energy Supply'] *= 1000000  
    #rename countries
    for var in range(0,len(energy)):
        if(energy.iloc[var,0]=="Republic of Korea"):
            energy.set_value(var, "Country", "South Korea")
        if(energy.iloc[var,0]=="United States of America"):
            energy.set_value(var, "Country", "United States")
        if(energy.iloc[var,0]=="United Kingdom of Great Britain and Northern Ireland"):
            energy.set_value(var, "Country", "United Kingdom")
        if(energy.iloc[var,0]=="China, Hong Kong Special Administrative Region"):
            energy.set_value(var, "Country", "Hong Kong")
   #remove brackets 
    for var in range(0,len(energy)):
       if("(" in energy.iloc[var,0]):
           line = energy.iloc[var,0].split()
           energy.iloc[var,0] = ""
           for item in line:
               if("(" in item):
                   energy.iloc[var,0] = energy.iloc[var,0].strip()
                   break
               energy.iloc[var,0] = energy.iloc[var,0] + item + " "
    #GDP
    gdp = pd.read_csv("world_bank.csv", header=4)
    
    #rename countries
    position = 0
    for country in gdp.iloc[:,0]:
        if(country=="Korea, Rep."):
            gdp.iloc[position, 0] = "South Korea"
        if(country=="Iran, Islamic Rep."):
            gdp.iloc[position, 0] = "Iran"
        if(country=="Hong Kong SAR, China"):
            gdp.iloc[position, 0] = "Hong Kong"
        position = position + 1
    #ScimEN 
    ScimEN = pd.read_excel("scimagojr-3.xlsx") 
       
    #Extract parts of the tables 
    gdp.drop(gdp.columns[1:49],axis=1,inplace=True)
    del(gdp["2005"])
    #gdp = gdp.set_index("Country Name")
    gdp.columns = ["Country", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015"]
       
    #removing countries from gdp which are not in energy
    position = 0
    countries = []
    drop_list = []
    pre = len(gdp)
    for country in energy.iloc[:,0]:
        countries.append(country)
    
    for country in gdp.iloc[:,0]:
        if(country not in countries):
            drop_list.append(position)
        position = position + 1
    
    gdp = gdp.drop(gdp.index[[drop_list]])
    post = len(gdp)
    gdp_difference = pre-post
        
    #removing countries from energy which are not in gdp 
    position = 0
    countries = []
    drop_list = []
    pre = len(energy)
    for country in gdp.iloc[:,0]:
        countries.append(country)
    
    for country in energy.iloc[:,0]:
        if(country not in countries):
            drop_list.append(position)
        position = position + 1
    
    energy = energy.drop(energy.index[[drop_list]])
    post = len(energy)
    energy_difference = pre-post
    
    ##this is for question 2. How many countries were lost from ScimEN
   
    position = 0
    countries = []
    drop_list = []
    pre = len(ScimEN)
    for country in energy.iloc[:,0]:
        countries.append(country)
    
    for country in ScimEN.iloc[:,1]:
        if(country not in countries):
            drop_list.append(position)
        position = position + 1
    
    #edditing ScimEN
    ScimEN = ScimEN.drop(ScimEN.index[[drop_list]])
    post = len(ScimEN)
    ScimEN_difference = pre-post
    ScimEN.to_csv("ScimEN.csv")
    
    
    ScimEN = ScimEN.drop(ScimEN.index[15:len(ScimEN)])
    ScimEN = ScimEN.set_index("Country")
    ScimEN.sort_index(inplace=True)
    
    #merging data
    temp = pd.merge(energy, gdp, on="Country")
    temp = temp.set_index("Country")
    result = pd.merge(temp, ScimEN,  how="inner",left_index=True, right_index=True)       
      
    #rearrange columns    
    cols = ['Rank', 'Documents', 'Citable documents', 'Citations', 'Self-citations', 'Citations per document', 'H index', 'Energy Supply', 'Energy Supply per Capita', '% Renewable', '2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015']
    result = result[cols]   
    
    
    return result
answer_one()  

def answer_two():
    ######
    return
#answer_two()

def answer_three():
    data = answer_one()
    #create Average column and sort 
    data["Average"] = 0.1*(data["2006"] + data["2007"] + data["2008"] + data["2009"] + data["2010"] + data["2011"] + data["2012"] + data["2013"] + data["2014"] + data["2015"])
    data = data.sort_values(['Average'], ascending=[False])
    #drop unnecessary columns
    data.drop(data.columns[0:20],axis=1,inplace=True)
   
    result = data.head(15)
    #converts df to Series
    avgGDP = result.T.squeeze()
       
    return avgGDP
#answer_three()

def answer_four():
    data = answer_one()
    #create Average column and sort 
    data["Average"] = 0.1*(data["2006"] + data["2007"] + data["2008"] + data["2009"] + data["2010"] + data["2011"] + data["2012"] + data["2013"] + data["2014"] + data["2015"])
    #resetting the index enables search through iloc
    data = data.reset_index()
    data = data.sort_values(['Average'], ascending=[False])
    #retrieve particular data
    result = data.iloc[5,20] - data.iloc[5,11]
    return result
#answer_four()

def answer_five():
    data = answer_one()
    total = data["Energy Supply per Capita"].sum()
    mean = total/15
       
    return mean
#answer_five()
    
def answer_six():
    data = answer_one()
    data = data.reset_index()
    data = data.sort_values(['% Renewable'], ascending=[False])
    result = (data.iloc[0,0]), (data.iloc[0,10])
    
    return result
#answer_six()

def answer_seven():
    data = answer_one()
    data["Ratio"] = data["Self-citations"]/data["Citations"]
    data = data.reset_index()
    data = data.sort_values(['Ratio'], ascending=[False])
    result = (data.iloc[0,0]), (data.iloc[0,21])
    
    return result
#answer_seven()

def answer_eight():
    data = answer_one()
    data["Pop estimate"] = data["Energy Supply"]/data["Energy Supply per Capita"]
    data = data.reset_index()
    data = data.sort_values(['Pop estimate'], ascending=[False])
    result = data.iloc[2,0]

    return result
#answer_eight()

def answer_nine():   
    #from previous question. Get pop estimate
    data = answer_one()
    data["Pop estimate"] = data["Energy Supply"]/data["Energy Supply per Capita"]
    data = data.reset_index()
    data = data.sort_values(['Pop estimate'], ascending=[False])
    #Create new column
    data["Citable documents per capita"] = data["Citable documents"]/data["Pop estimate"]
    #calculate correlation
    correlation = data["Citable documents per capita"].corr(data["Energy Supply per Capita"])
    
    return correlation
#answer_nine()

def answer_ten():
    data = answer_one()
    #calculate median 
    median = data["% Renewable"].median()
    #create new column
    data["HighRenew"] = np.NaN
    #change NaN to one if condition satisfied 
    for var in range(0, len(data)):
        if(data.iloc[var,9]>=median):
            data.iloc[var, 20]= float(1)
    #sort by rank
    data = data.sort_values(['Rank'], ascending=[True])
    result = data["HighRenew"]
      
    return result

answer_ten()    

def answer_eleven():
    data = answer_one()
    data["Pop estimate"] = data["Energy Supply"]/data["Energy Supply per Capita"]
    ContinentDict  = {'China':'Asia', 'United States':'North America', 
                  'Japan':'Asia', 'United Kingdom':'Europe', 'Russian Federation':'Europe', 'Canada':'North America', 
                  'Germany':'Europe', 'India':'Asia', 'France':'Europe', 
                  'South Korea':'Asia', 'Italy':'Europe', 'Spain':'Europe', 
                  'Iran':'Asia', 'Australia':'Australia', 'Brazil':'South America'}
    
    #Create a continent column and fill it with the dictionary content
    data["Continent"] = np.nan
    data["Continent"] = data["Continent"].fillna(ContinentDict, axis = 0)
    #make an empty df and fill it with the appropriate data
    df = pd.DataFrame()
    #establish index
    df["Continent"] = ('Asia', 'Australia', 'Europe', 'North America', 'South America')
    df = df.set_index("Continent")
    df = df.sort_index()
    #establish columns
    df["size"] = np.nan
    df["sum"] = 0
    df["mean"] = 0
    df["std"] = 0
    #fill size
    df.iloc[0,0] = sum(data["Continent"]=="Asia")
    df.iloc[1,0] = sum(data["Continent"]=="Australia")
    df.iloc[2,0] = sum(data["Continent"]=="Europe")
    df.iloc[3,0] = sum(data["Continent"]=="North America")
    df.iloc[4,0] = sum(data["Continent"]=="South America")
    #fill sum
    for var in range(0,len(data)):
        if(data.iloc[var,21]=="Asia"):
            df.iloc[0,1]=df.iloc[0,1] + data.iloc[var,20]
        if(data.iloc[var,21]=="Australia"):
            df.iloc[1,1]=df.iloc[1,1] + data.iloc[var,20]
        if(data.iloc[var,21]=="Europe"):
            df.iloc[2,1]=df.iloc[2,1] + data.iloc[var,20]
        if(data.iloc[var,21]=="North America"):
            df.iloc[3,1]=df.iloc[3,1] + data.iloc[var,20]
        if(data.iloc[var,21]=="South America"):
            df.iloc[4,1]=df.iloc[4,1] + data.iloc[var,20]
    #fill mean
    for var in range(0,len(df)):
        df.iloc[var,2] = df.iloc[var,1]/df.iloc[var,0]
    #fill std
    for var in range(0,len(df)):
        df.iloc[var,3] = ((df.iloc[var,1]-df.iloc[var,2])**2/(df.iloc[var,0]-1))**0.5
    print(df)
    return df

#answer_eleven()

#Convert the Population Estimate series to a string with 
#thousands separator (using commas) e.g. 12345678.90 -> 12,345,678.90
#This function should return a Series PopEst whose index
#is the country name and whose values are the population estimate string.

def answer_thirteen():
    #load data
    data = answer_one()
    data["Pop estimate"] = data["Energy Supply"]/data["Energy Supply per Capita"]
    #set index to country
    data = data.reset_index()
    data = data.set_index("Country")
    #reduce data to a series with Pop Estimate
    data = data["Pop estimate"]
    
    #change pop estimate volue to string with commas 
    for var in range(0,len(data)):
        line = str(data.iloc[var])
        #separate decimal point
        line = line.split(".")
        
        temp = []
        backwards = []
        ordered = []
        count = 1
        #load the number, traverse it and add commas
        for char in line[0]:
            temp.append(char)
        for i in range(0,len(line[0])):
            backwards.append(temp[len(line[0])-1-i])
            if(count==3):
                if(i==len(line[0])-1):
                    break
                backwards.append(",")
                count = 0                
            count = count + 1
        
        #traverse it back, concatenate, add decimal point and the number after it 
        for char in reversed(backwards):
            ordered.append(char)
        ordered = "".join(ordered)
        newValue = ordered + "." + line[1] 
        
        #change the value in the serie
        data.iloc[var] = newValue
        
    #print(data)
    return data
    
answer_thirteen()

    
    