http://www.codeskulptor.org/#user43_7QmCifaweE_29.py
"""
Planner for Yahtzee
Simplifications:  only allow discard and roll, only score against upper level
"""

# Used to increase the timeout, if necessary
import codeskulptor
codeskulptor.set_timeout(5)

def gen_all_sequences(outcomes, length):
    """
    Iterative function that enumerates the set of all sequences of
    outcomes of given length.
    """
    
    answer_set = set([()])
    for dummy_idx in range(length):
        temp_set = set()
        for partial_sequence in answer_set:
            for item in outcomes:
                new_sequence = list(partial_sequence)
                new_sequence.append(item)
                temp_set.add(tuple(new_sequence))
        answer_set = temp_set
    return answer_set

def score(hand):
    """
    Compute the maximal score for a Yahtzee hand according to the
    upper section of the Yahtzee score card.

    hand: full yahtzee hand

    Returns an integer score 
    """
    hand = list(hand)
    #The set tells us which numbers are contained in hand 
    hand_set  = set(hand)
    scores = []    
    temp_sum = 0
    
    #get sum for each number in hand
    for number in hand_set:
        for element in hand:
            if(number==element):
                temp_sum += element
                
        scores.append(temp_sum)
        temp_sum = 0
      
    return max(scores)

def expected_value(held_dice, num_die_sides, num_free_dice):
    """
    Compute the expected value based on held_dice given that there
    are num_free_dice to be rolled, each with num_die_sides.

    held_dice: dice that you will hold
    num_die_sides: number of sides on each die
    num_free_dice: number of dice to be rolled

    Returns a floating point expected value
    """  	 
 
    expected_hands = []
    score_sum = []
    temp = []
    
    #generat all possible outcomes for the remaining dice
    outcomes = gen_all_sequences(range(1,num_die_sides+1), num_free_dice)
        
    #make a final hand from possible outcome and held dice
    for outcome in outcomes:
        
        for number in outcome:
            temp.append(number)
        
        for number in held_dice:
            temp.append(number)
        
        expected_hands.append(tuple(temp))
        del temp[:]
        
    #compute score for all possible hands
    for hand in expected_hands:
        score_sum.append(score(hand))
    
    #get expected value
    expec_value = sum(score_sum)/float(len(score_sum))
    
    return expec_value

def gen_all_holds(hand):
    """
    Generate all possible choices of dice from hand to hold.

    hand: full yahtzee hand

    Returns a set of tuples, where each tuple is dice to hold
    """
    
    #add possible combinations. Sorting prevents duplication
    result = set()
    for var in range(1,len(hand)+1):
        for item in gen_all_sequences(hand, var):
            item = sorted(list(item))
            result.add(tuple(item))        
    
    
    #get occurences of numbers in hand
    hand = list(hand)
    occurence = []
    drop = set()
           
    for var in range (len(hand)):
        occurence.append(hand.count(hand[var]))    
          
    #Note combinations when a single die is used multiple times
    for var in range(len(hand)): 
        for item in result:                    
            if(list(item).count(hand[var])>occurence[var]):
                drop.add(item)
    
    #Erase these combinations
    for item in list(drop):
        result.remove(item)
    
    #account for possibility of not holding any die
    result.add(())
    
    return result

def strategy(hand, num_die_sides):
    """
    Compute the hold that maximizes the expected value when the
    discarded dice are rolled.

    hand: full yahtzee hand
    num_die_sides: number of sides on each die

    Returns a tuple where the first element is the expected score and
    the second element is a tuple of the dice to hold
    """
    
    #calculate scores of all possible hands
    possible_hand = []
    all_holds = list(gen_all_holds(hand))
    
    for item in all_holds:
        possible_hand.append(expected_value(item, num_die_sides, len(hand) - len(item)))
    
    #get maximum score
    position = possible_hand.index(max(possible_hand))
    
    best_hand = all_holds[position]
    best_value = max(possible_hand)
    
    return (best_value, best_hand)

def run_example():
    """
    Compute the dice to hold and expected score for an example hand
    """
    num_die_sides = 6
    hand = (1, 1, 1, 5, 6)
    hand_score, hold = strategy(hand, num_die_sides)
    print "Best strategy for hand", hand, "is to hold", hold, "with expected score", hand_score
    
    





