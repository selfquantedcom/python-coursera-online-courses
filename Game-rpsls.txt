http://www.codeskulptor.org/#user43_kISePYZCAG_27.py
# Rock-paper-scissors-lizard-Spock template
import random 

# The key idea of this program is to equate the strings
# "rock", "paper", "scissors", "lizard", "Spock" to numbers
# as follows:
#
# 0 - rock
# 1 - Spock
# 2 - paper
# 3 - lizard
# 4 - scissors

# helper functions

def name_to_number(name):
    out = None
    if(name=="rock"):
        out = 0
    elif(name=="Spock"):
        out = 1
    elif(name=="paper"):
        out = 2
    elif(name=="lizard"):
        out = 3
    elif(name=="scissors"):
        out = 4
    else:
        print("Invalid name")
    return out


def number_to_name(number):
    out = None
    if(number==0):
        out = "rock"
    elif(number==1):
        out = "Spock"
    elif(number==2):
        out = "paper"
    elif(number==3):
        out = "lizard"
    elif(number==4):
        out = "scissors"
    else:
        print("Invalid number")
    return out
    

def rpsls(player_choice): 
    # print a blank line to separate consecutive games
    print "\n"
   
    # print out the message for the player's choice
    print "Player chooses " + player_choice
    
    # convert the player's choice to player_number using the function name_to_number()
    player_number = name_to_number(player_choice)
    
    # compute random guess for comp_number using random.randrange()
    comp_number = random.randrange(0,5)
        
    # convert comp_number to comp_choice using the function number_to_name()
    comp_choice = number_to_name(comp_number)
    
    # print out the message for computer's choice
    print "Computer chooses: " + comp_choice   

    # compute difference of comp_number and player_number modulo five
    diff = (comp_number - player_number)%5   
    # use if/elif/else to determine winner, print winner message
    if(diff==0):
        print "Player and Computer tie!"
    elif(diff==1 or diff==2):
        print "Computer wins!"
    else:
        print "Player winns!" 
        
    
# test your code - THESE CALLS MUST BE PRESENT IN YOUR SUBMITTED CODE
rpsls("rock")
rpsls("Spock")
rpsls("paper")
rpsls("lizard")
rpsls("scissors")

# always remember to check your completed program against the grading rubric


