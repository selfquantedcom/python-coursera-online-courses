# -*- coding: utf-8 -*-
"""
Created on Thu Apr 27 14:01:08 2017

@author: Frantisek
"""
import pandas as pd
import numpy as np
from scipy.stats import ttest_ind

#read and clean data
uniTowns = open('university_towns.txt')
housePrices = pd.read_csv("City_Zhvi_AllHomes.csv")
gdp = pd.read_excel("gdplev.xls", header = 8)
gdp.drop(gdp.columns[[0,1,2,3,5,7]], axis=1, inplace=True)
gdp.columns=["Quarter","GDP" ]

#get list of university towns
townAndState = []
count = 0
for line in uniTowns:
    #append a tuple with (State,Town)
    if("(" not in line):
        #erase "["
        if("[" in line):
            temp = ""
            for char in line:
                if(char=="["):
                    line = temp
                    stateName = line.strip() 
                    break
                temp = temp + char
        else:
            townName = line.strip()
    else:
        townName = line.split("(")[0].strip()
    
    if(count==0):
        count = 1
        continue
    try:
        if(townAndState[-1][1]==townName):
            continue
        else:
            townAndState.append((stateName, townName))
    except:
        townAndState.append((stateName, townName))

#insert data into df 
columns = ["State","RegionName"]
uniTowns = pd.DataFrame(townAndState, columns=columns)
del townName, stateName, line, char, count

#identify start of recession
recStart = [0]
inRecession = False
for i in range(0,len(gdp)-1):
    if(inRecession == False):
        if(gdp.iloc[i+1,1] < gdp.iloc[i,1] and gdp.iloc[i+1,1] > gdp.iloc[i+2,1]):
             recStart.append(gdp.iloc[i+1,0])
             inRecession = True
    else:
        if(gdp.iloc[i,1] > gdp.iloc[i-1,1] and gdp.iloc[i-1,1] < gdp.iloc[i-2,1]):
            inRecession = False

recStart = recStart[-1]
del inRecession
#identify end of recession 
recEnd = []
for i in range(0,len(gdp)):
        if(gdp.iloc[i,0]==recStart):
                for var in range(i,len(gdp)-2):
                    if(gdp.iloc[var+1,1] > gdp.iloc[var,1] and gdp.iloc[var+1,1] < gdp.iloc[var+2,1]):
                        recEnd.append(gdp.iloc[var+2,0])
                        break
recEnd = recEnd[-1]              

#find a quarter with the lowest GDP within a recession   
recBottom = []
temp = []

for i in range(0,len(gdp)):
        if(gdp.iloc[i,0]==recStart):
                for var in range(i,len(gdp)):
                    temp.append((gdp.iloc[var,1],gdp.iloc[var,0]))
                    if(gdp.iloc[var,0]==recEnd):
                        recBottom.append(min(temp))
                        del(temp[:])
                        break
recBottom = recBottom[-1][1]
del temp, var, i
#drop columns in house
states = {'OH': 'Ohio', 'KY': 'Kentucky', 'AS': 'American Samoa', 'NV': 'Nevada', 'WY': 'Wyoming', 'NA': 'National', 'AL': 'Alabama', 'MD': 'Maryland', 'AK': 'Alaska', 'UT': 'Utah', 'OR': 'Oregon', 'MT': 'Montana', 'IL': 'Illinois', 'TN': 'Tennessee', 'DC': 'District of Columbia', 'VT': 'Vermont', 'ID': 'Idaho', 'AR': 'Arkansas', 'ME': 'Maine', 'WA': 'Washington', 'HI': 'Hawaii', 'WI': 'Wisconsin', 'MI': 'Michigan', 'IN': 'Indiana', 'NJ': 'New Jersey', 'AZ': 'Arizona', 'GU': 'Guam', 'MS': 'Mississippi', 'PR': 'Puerto Rico', 'NC': 'North Carolina', 'TX': 'Texas', 'SD': 'South Dakota', 'MP': 'Northern Mariana Islands', 'IA': 'Iowa', 'MO': 'Missouri', 'CT': 'Connecticut', 'WV': 'West Virginia', 'SC': 'South Carolina', 'LA': 'Louisiana', 'KS': 'Kansas', 'NY': 'New York', 'NE': 'Nebraska', 'OK': 'Oklahoma', 'FL': 'Florida', 'CA': 'California', 'CO': 'Colorado', 'PA': 'Pennsylvania', 'DE': 'Delaware', 'NM': 'New Mexico', 'RI': 'Rhode Island', 'MN': 'Minnesota', 'VI': 'Virgin Islands', 'NH': 'New Hampshire', 'MA': 'Massachusetts', 'GA': 'Georgia', 'ND': 'North Dakota', 'VA': 'Virginia'}
#drop columns in house
housePrices["State"].replace(states, inplace=True)
housePrices.set_index(['State', 'RegionName'], inplace=True)
housePrices.drop(housePrices.columns[0:49], axis=1, inplace=True)

#get years and quarters
years = []
for column in housePrices:
    years.append(column)
quarters = []
for i in range(211,len(gdp)):
    quarters.append(gdp.iloc[i,0])
quarters.append("2016q3")
#create quarters columns in house
for i in range(1,len(quarters)):
    if(i==len(quarters)-1):
        housePrices[quarters[i-1]] = housePrices[[years[3*i-3], years[3*i-2], years[3*i-1]]].mean(axis=1)
        housePrices[quarters[i]] = housePrices[[years[3*i], years[3*i+1]]].mean(axis=1)
        break
    housePrices[quarters[i-1]] = housePrices[[years[3*i-3], years[3*i-2], years[3*i-1]]].mean(axis=1)

del years, quarters
#drop original "year" columns
housePrices.drop(housePrices.columns[0:200], axis = 1, inplace=True)

###ttest
#find the quarter before the recession 
preRecession = 0
for column in housePrices:
    if(column==recStart):
        break
    preRecession = preRecession + 1
#find recession bottom 
recBotIndex = 0
for column in housePrices:
    if(column == recBottom):
        break
    recBotIndex = recBotIndex + 1
    
#price_ratio=quarter_before_recession/recession_bottom #the quarter before rec: 2008q2
housePrices["priceRatio"] = housePrices["2008q3"].div(housePrices[recBottom])
housePrices  = housePrices["priceRatio"].dropna().reset_index()


#iterate through housePrices rows. Iterate through townAndState items.
#when item matches, append it to uniPriceRatio. Delete that row by index (use counter)
#When done only non uni towns are left. Append to ordinaryPrice Ratio.
#run ttest

def run_ttest():
    
    #distinguish university towns from ordinary towns in housePrices
    uniPriceRatio = []
    ordinaryPriceRatio = []
    dropList = []
    index = 0
    
    #store price ratio of uni towns
    for row in housePrices.itertuples():
        for item in townAndState:
            if(item[1] == row[2] and item[0] == row[1]):
                uniPriceRatio.append(row[-1])
                dropList.append(index)
   
        index = index + 1    
    
    data = housePrices.drop(housePrices.index[dropList])         
    
    #store price ratio of ordinary towns
    for row in data.itertuples():
        ordinaryPriceRatio.append(row[-1])
        
    #ttest
    #print(sum(ordinaryPriceRatio)/len(ordinaryPriceRatio))
    #print(sum(uniPriceRatio)/len(uniPriceRatio))
    result = (True,ttest_ind(uniPriceRatio, ordinaryPriceRatio)[-1] , "university town")
    
    return result
run_ttest()






             
